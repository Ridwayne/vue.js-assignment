import firebase from "@firebase/app";
import "firebase/firestore";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCnbIyWQ4WO5iR52y9_ft60Z_1kG7beGlg",
    authDomain: "bloodgroup-aa285.firebaseapp.com",
    databaseURL: "https://bloodgroup-aa285.firebaseio.com",
    projectId: "bloodgroup-aa285",
    storageBucket: "bloodgroup-aa285.appspot.com",
    messagingSenderId: "297813162151",
    appId: "1:297813162151:web:11b7538f30737ddfe64382"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

export { db };
